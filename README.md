# GitLab CI Templates

## Example Runner

```sh
docker network create gitlab

docker run -d --name minio --restart=always -p 9000:9000 \
  -v /gitlab-runner/minio:/data \
  cr.io/minio/minio:RELEASE.2021-06-17T00-10-46Z server /data

docker run -d --name gitlab-runner --restart=always \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v /gitlab-runner/config:/etc/gitlab-runner \
  -v /rootCA.crt:/etc/gitlab-runner/certs/ca.crt \
  -e TZ=Asia/Singapore \
  cr.io/gitlab/gitlab-runner:alpine
```

## Runner Config

Following example config allows running `docker-in-docker`.

```toml
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "my-runner-name"
  url = "https://gitlab.dev.local/"
  token = "**********"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    Type = "s3"
    Path = "my-path"
    Shared = true
    [runners.cache.s3]
      ServerAddress = "host.docker.internal:9000"
      AccessKey = "******"
      SecretKey = "******""
      BucketName = "runner"
      Insecure = true
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "cr.io/gitlab/gitlab-runner:alpine"
    privileged = true
    disable_entrypoint_overwrite = false
    helper_image = "cr.io/gitlab-org/gitlab-runner/gitlab-runner-helper:x86_64-f761588f"
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache", "/certs/client"]
    shm_size = 0
```
